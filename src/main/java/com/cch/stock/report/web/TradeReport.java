package com.cch.stock.report.web;

import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cch.platform.web.WebUtil;
import com.cch.stock.report.service.TradeReportService;

@Controller
@RequestMapping(value = "/report/tradereport")
public class TradeReport {

	@Autowired
	private TradeReportService service;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pagequery.do")
	public String  pageQuery(ServletRequest request,ModelMap mm) throws Exception {
		Map<String, Object> param=WebUtil.getParam(request);
		mm.putAll(service.getData(param));
		return "jsonView";
	}

}
