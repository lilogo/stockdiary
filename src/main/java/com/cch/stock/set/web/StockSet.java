package com.cch.stock.set.web;

import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.dao.PageObject;
import com.cch.platform.web.WebUtil;
import com.cch.stock.set.bean.SdStock;
import com.cch.stock.set.service.StockService;

@Controller
@RequestMapping(value = "/stock/stockset")
public class StockSet {

	@Autowired
	private StockService service;
	
	@RequestMapping(value = "/pagequery.do")
	public ModelAndView  pageQuery(ServletRequest request) {
		Map<String, Object> param=WebUtil.getParam(request);
		return new ModelAndView("jsonView",service.pageQuery(param));
	}

	@RequestMapping(value = "/save.do")
	public String save(SdStock stock,ServletRequest request) {
		service.saveOrUpdate(stock);
		return ("jsonView");
	}
	
	@RequestMapping(value = "/delete.do")
	public String delete(SdStock stock,ServletRequest request) {
		service.delete(stock);
		return ("jsonView");
	}
}
