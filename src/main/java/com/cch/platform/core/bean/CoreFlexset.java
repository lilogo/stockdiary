package com.cch.platform.core.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * CoreFlexset entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "core_flexset")
public class CoreFlexset implements java.io.Serializable {

	// Fields

	private String flexsetCode;
	private String flexsetName;
	private String type;
	private String querySql;

	// Constructors

	/** default constructor */
	public CoreFlexset() {
	}

	/** minimal constructor */
	public CoreFlexset(String flexsetName, String type) {
		this.flexsetName = flexsetName;
		this.type = type;
	}

	/** full constructor */
	public CoreFlexset(String flexsetName, String type, String querySql) {
		this.flexsetName = flexsetName;
		this.type = type;
		this.querySql = querySql;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "flexset_code", unique = true, nullable = false, length = 100)
	public String getFlexsetCode() {
		return this.flexsetCode;
	}

	public void setFlexsetCode(String flexsetCode) {
		this.flexsetCode = flexsetCode;
	}

	@Column(name = "flexset_name", nullable = false, length = 100)
	public String getFlexsetName() {
		return this.flexsetName;
	}

	public void setFlexsetName(String flexsetName) {
		this.flexsetName = flexsetName;
	}

	@Column(name = "type", nullable = false, length = 45)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "query_sql", length = 1000)
	public String getQuerySql() {
		return this.querySql;
	}

	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}

}