package com.cch.platform.base.web;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.base.bean.BaseMenu;
import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.base.service.MenuService;
import com.cch.platform.util.JsonTreeUtil;
import com.cch.platform.web.WebUtil;


@Controller
@RequestMapping(value = "/base/menucontroller")
public class MenuController {

	@Autowired
	MenuService ms=null;
	
	@RequestMapping(value = "/query.do")
	public String query(HttpServletRequest request,ModelMap mm) {
		BaseUser user=(BaseUser)request.getSession().getAttribute("user");
		List menus=ms.getAllMenu(user);
		Set tree=JsonTreeUtil.getTree(menus, "code", "pcode", "sort","name");
		mm.put("menus", tree);
		return "jsonView";
	}
	
	@RequestMapping(value = "/getbycode.do")
	public String getByCode(HttpServletRequest request,ModelMap mm){
		String code=request.getParameter("code");
		mm.put("menu", ms.get(code));
		return "jsonView";
	}
	
	@RequestMapping(value = "/save.do")
	public  ModelAndView save(BaseMenu menu,HttpServletRequest request){
		ms.saveOrUpdate(menu);
		return WebUtil.sucesseView("保存成功！");
	}
	
	@RequestMapping(value = "/delete.do")
	public  ModelAndView delete(BaseMenu menu){
		ms.delete(menu);
		return WebUtil.sucesseView("保存成功！");
	}
}
